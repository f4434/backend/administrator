package com.devups.administrator.core;

import java.util.List;
import java.util.Optional;

import com.devups.administrator.model.OrdensEntity;

public interface OrdensCore {
    List<OrdensEntity> findAll();
    OrdensEntity create(OrdensEntity ordensEntity);
    Optional<OrdensEntity> update(OrdensEntity ordensEntity);
    Boolean delete(Long id);
    OrdensEntity findById(Long id);
    List<OrdensEntity> findByOrden(String id);
}
