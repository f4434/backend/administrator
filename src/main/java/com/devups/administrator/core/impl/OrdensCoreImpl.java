package com.devups.administrator.core.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.devups.administrator.core.OrdensCore;
import com.devups.administrator.model.HistoryOrdenEntity;
import com.devups.administrator.model.Orden;
import com.devups.administrator.model.OrdensEntity;
import com.devups.administrator.repository.OrdenOneRepository;
import com.devups.administrator.repository.OrdensRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrdensCoreImpl implements OrdensCore {

    @Autowired
    OrdensRepository ordensRepository;

    @Autowired
    OrdenOneRepository ordenOneRepository;

    @Override
    public List<OrdensEntity> findAll() {
        List<HistoryOrdenEntity> ordensEntities = new ArrayList<>();
        ordensRepository.findAll().forEach(ordensEntities::add);
        List<OrdensEntity> dOrdensEntities = new ArrayList<>();
        List<Orden> orden = new ArrayList<>();
        ordenOneRepository.findAll().forEach(orden::add);
        for (HistoryOrdenEntity historyOrdenEntity : ordensEntities) {
            OrdensEntity dto = new OrdensEntity();
            var sdds = orden.stream().filter(ee-> ee.getNumeroOrden().equals(historyOrdenEntity.getNumeroOrden())).collect(Collectors.toList());
            var transform = toTransformEntity(historyOrdenEntity, dto,sdds);
            dOrdensEntities.add(transform);
        }
        return dOrdensEntities;
    }

    public List<Orden> findAllOrden(String numeroOrden) {
        List<Orden> ordensEntities = new ArrayList<>();
        ordenOneRepository.findAll().forEach(ordensEntities::add);
        var listaFiltrada = ordensEntities.stream().filter(value -> value.getNumeroOrden().equals(numeroOrden)).collect(Collectors.toList());
        return listaFiltrada;
    }


    @Override
    public OrdensEntity create(OrdensEntity ordensEntity) {
        ordensEntity.setTotalAmount(calculeAmount(ordensEntity.getOrdens()));
        var ordenOnly = createOrden(ordensEntity.getOrdens(), ordensEntity.getNumeroOrden());
        HistoryOrdenEntity historyOrdenEntity = new HistoryOrdenEntity();
        OrdensEntity ordensEntity2 = new OrdensEntity();
        var restul = toTransformHistory(historyOrdenEntity, ordensEntity);
        var bdCreade = ordensRepository.save(restul);
        return toTransformEntity(bdCreade, ordensEntity2, ordenOnly);
    }

    List<Orden> createOrden(List<Orden> ordenList, String numeroOrden){
        List<Orden> lista = new ArrayList<>();
        for (Orden orden : ordenList) {
            orden.setNumeroOrden(numeroOrden);
            var rrr =ordenOneRepository.save(orden);
            lista.add(rrr);
        }
        return lista;
    }

    @Override
    public Optional<OrdensEntity> update(OrdensEntity ordensEntity) {
        ordensEntity.setTotalAmount(calculeAmount(ordensEntity.getOrdens()));
        /*return ordensRepository.findById(ordensEntity.getId())
        .map(old -> toTransform(old, ordensEntity))
        .map(ordensRepository::save);*/

        return null;
    }

    @Override
    public Boolean delete(Long id) {
        var ordenGeneral = ordensRepository.findById(id);
        findAllOrden(ordenGeneral.get().getNumeroOrden()).forEach(this::deleteOrdens);
        ordensRepository.deleteById(id);
        return true;
    }

    @Override
    public OrdensEntity findById(Long id) {
        OrdensEntity ordensEntity2 = new OrdensEntity();
        var xxxx = ordensRepository.findById(id);
        var yyyy = findAllOrden(xxxx.get().getNumeroOrden());
        return toTransformEntity(xxxx.get(), ordensEntity2, yyyy);  
    }

    OrdensEntity toTransform(OrdensEntity old, OrdensEntity nuw){
        old.setNameClient(nuw.getNameClient());
        old.setNameOperator(nuw.getNameOperator());
        old.setNumeroOrden(nuw.getNumeroOrden());
        old.setOrdens(nuw.getOrdens());
        old.setTotalAmount(nuw.getTotalAmount());
        return old;
    }

    Integer calculeAmount(List<Orden> ordenList){
        Integer amount = 0;
        for (Orden orden : ordenList) {
            amount += orden.getMonto();
        }
        return amount;
    }

    void deleteOrdens(Orden orden){
        ordenOneRepository.delete(orden);
    }


    HistoryOrdenEntity toTransformHistory(HistoryOrdenEntity old, OrdensEntity nuw){
        old.setNameClient(nuw.getNameClient());
        old.setNameOperator(nuw.getNameOperator());
        old.setNumeroOrden(nuw.getNumeroOrden());
        old.setTotalAmount(nuw.getTotalAmount());
        old.setNumeroMesa(nuw.getNumeroMesa());
        return old;
    }

    OrdensEntity toTransformEntity(HistoryOrdenEntity bd, OrdensEntity dto, List<Orden> ordens){
        dto.setNameClient(bd.getNameClient());
        dto.setNameOperator(bd.getNameOperator());
        dto.setNumeroOrden(bd.getNumeroOrden());
        dto.setTotalAmount(bd.getTotalAmount());
        dto.setNumeroMesa(bd.getNumeroMesa());
        dto.setId(bd.getId());
        dto.setOrdens(ordens);
        return dto;
    }

    @Override
    public List<OrdensEntity> findByOrden(String id) {
        List<HistoryOrdenEntity> ordensEntities = new ArrayList<>();
        ordensRepository.findAll().forEach(ordensEntities::add);
        var sss = ordensEntities.stream().filter(ee -> ee.getNumeroOrden().equals(id)).collect(Collectors.toList());
        List<Orden> orden = new ArrayList<>();
        ordenOneRepository.findAll().forEach(orden::add);
        var xxx = orden.stream().filter(ee -> ee.getNumeroOrden().equals(id)).collect(Collectors.toList());

        List<OrdensEntity> dOrdensEntities = new ArrayList<>();
        for (HistoryOrdenEntity cccc : sss) {
            OrdensEntity dto = new OrdensEntity();
            var transform = toTransformEntity(cccc, dto, xxx);
            dOrdensEntities.add(transform);
        }
        return dOrdensEntities;
    }

    
}
