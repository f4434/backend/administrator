package com.devups.administrator.repository;

import com.devups.administrator.model.Orden;
import org.springframework.data.repository.CrudRepository;

public interface OrdenOneRepository extends CrudRepository<Orden, Long>{
    
}
