package com.devups.administrator.model;

import java.util.List;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class OrdensEntity {
    private Long id;
    private String nameClient;
    private String numeroMesa;
    private String nameOperator;
    private List<Orden> ordens;
    private Integer totalAmount;
    private String numeroOrden;
    
}
