package com.devups.administrator.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "HISTORICO")
@Getter
@Setter
public class HistoryOrdenEntity {
    @Id
    @SequenceGenerator(
            name = "sq_sequence",
            sequenceName = "sq_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            generator = "sq_sequence",
            strategy = GenerationType.SEQUENCE
    )
    private Long id;
    private String nameClient;
    private String numeroMesa;
    private String nameOperator;
    private Integer totalAmount;
    private String numeroOrden;
    
}
